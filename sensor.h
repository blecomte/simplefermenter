// sensor.h

#ifndef _SENSOR_h
#define _SENSOR_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif


/*
* Init all the sensors
*/
void sensor_init(void);

/*
* Run all the sensors. Read them all and spit it into a queue for the uploader
*/
void sensor_run(void); 

#endif

