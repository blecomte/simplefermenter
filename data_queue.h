// data_queue.h

#ifndef _DATA_QUEUE_h
#define _DATA_QUEUE_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
#include "sensors.h"

void data_queue_init(void);
void data_push(sensors_t s);
sensors_t data_pop(void);
bool data_isEmpty(void);


#endif

