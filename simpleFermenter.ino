#include "data_queue.h"
#include "uploader.h"
#include "sensor.h"
#include "sensors.h"

void setup()
{

	/* add setup code here */
	//debug_init();
	sensor_init();
	uploader_init();
	//ui_init();
	//control_init();
}

void loop()
{

  /* add main program code here */
	sensor_run();
	uploader_run();

}
