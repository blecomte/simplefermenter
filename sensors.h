#pragma once

#define SENSORS_MAX_SENSORS (2)

typedef struct {
	float int_temperature;
	float ext_temperature;
} sensors_t ;