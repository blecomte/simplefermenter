// uploader.h

#ifndef _UPLOADER_h
#define _UPLOADER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif



/*
* init the uploader
*/
void uploader_init(void);
/*
*	Check the time. Connect to the wifi and upload the data that is queued up
*/
void uploader_run(void);


#endif

