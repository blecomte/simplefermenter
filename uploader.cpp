// 
// 
// 

#include "uploader.h"

typedef enum {
	IDLE,
	POWERUP,
	CONNECT,
	UPLOAD,
	DISCONNECT
}uploader_state_e;

static uploader_state_e m_state;

void uploader_init(void)
{
	m_state = IDLE;
}

void uploader_run(void)
{
	switch (m_state)
	{
		case IDLE:
			/*if its time to upload begin the connection*/
			m_state = POWERUP;
			break;
		case POWERUP:
			/*turn on the network periph*/
			m_state = CONNECT;
			break;
		case CONNECT:
			/*turn connect to the network*/
			m_state = UPLOAD;
			break;
		case UPLOAD:
			/* upload all the data from the queue*/
			m_state = DISCONNECT;
			break;
		case DISCONNECT:
			/* disconnect from network and shut down periph*/
			m_state = IDLE;
			break;
		default:
			m_state = IDLE;
		break;
	}

}