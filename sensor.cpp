// 
// 
// 

#include "sensor.h"
#include "sensors.h"


typedef enum {
	IDLE,
	START,
	WAIT
} sensor_e;

typedef void* (*readPtr)(void* param); //ultra generic func ptr
typedef void* (*convPtr)(void* param); //ultra generic func ptr

typedef struct {
	readPtr read;
	convPtr conv;
	const char* name;
} sense_t;

static readPtr read_callbacks[SENSORS_MAX_SENSORS] = { NULL,NULL };
static convPtr conv_callbacks[SENSORS_MAX_SENSORS] = { NULL,NULL };
static const char* names[SENSORS_MAX_SENSORS] = { "internal","external" };
static sense_t m_all_sensors[SENSORS_MAX_SENSORS];
static sensor_e m_state;


void sensor_init(void)
{
	int i = 0;
	m_state = IDLE;
	for (i = 0; i < SENSORS_MAX_SENSORS; i++)
	{
		m_all_sensors[i].read = read_callbacks[i];
		m_all_sensors[i].conv = conv_callbacks[i];
		m_all_sensors[i].name = names[i];
	}
}

void sensor_run(void)
{
	switch (m_state)
	{
		case IDLE:
			m_state = START;
			break;
		case START:
			m_state = WAIT;
			break;
		case WAIT:
			m_state = IDLE;
			break;
		default:
			m_state = IDLE;
			break;

	}
}